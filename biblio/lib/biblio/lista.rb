Nodo=Struct.new(:n, :next)
Nodo2=Struct.new(:n, :prev, :next)

class Lista
    def initialize()
       @first=Nodo.new(nil)
    end
    
    def first
        @first
    end
    
    def extract_first()
        @aux=Nodo.new(nil)
        @aux=@first
        @first=@first.next
        @aux
    end
    
    def insert_first(nodo)
        nodo.next=@first
        @first=nodo
    end
    
    def insert_next(nodo1, nodo2)
       
        nodo1.next=nodo2
    end
end 

class Lista2
    
    attr_accessor :head, :tail
    
    def initialize()
        @tail=nil
        @head=nil
    end
    
   
    def extract_head()
        @aux=Nodo2(nil)
        @aux=@head
        @head=@head.prev
        @head.next=nil
        @aux
    end
    
    
    def insert_nodo(item)
        
        @aux= Nodo2.new(item,nil,nil)
       
       if @tail==nil || @head==nil
            @aux.prev=nil
            @aux.next=nil
            @head=@aux
            @tail=@aux
            @aux
        else
            @aux.prev=@head
            @head.next=@aux     
            @aux.next=nil
            @head=@aux
            @aux
        end
        
    end
    
end

