require 'spec_helper'
require 'biblio'


describe Bibliografia do
	before :each do
	@libro1=Libro.new(["Dave Thomas","Andy Hunt","Chad Fowler"], "The pragmatic programmers guide", "the facets of ruby", "Pragmatic Bookshelf", "4 edition", "July 7, 2013", ["isbn-13","isbn-10"])
	@libro2=Libro.new(["Scott Chacon"], "Pro git 2009 edition", "nil", "Apress", "9 edition", "August 27 2007", ["isbn-13","isbn-10"])
	@libro3=Libro.new(["David Flanagan","Yukihiro Matsumoto"], "The ruby programming language", "nil", "Oreilly media", "1 edition", "february4, 2008", ["isbn-13","isbn-10"])
	@libro4=Libro.new(["David Chelimsky","Dave Astels","Bryan Helkamp", "Dan North", "Zach Dennis", "Aslak Hellesoy"], "The Rspec book", "the facets of ruby", "Pragmatic Bookshelf", "1 edition", "December 25, 2010", ["isbn-13","isbn-10"])
	@libro5=Libro.new(["Richard E Silverman"], "Git pocket guide", "nil", "Oreilly media", "1 edition", "August 2, 2013", ["isbn-13","isbn-10"])
	
	@articulo1=Articuloe.new("Pepe", "Masacre en Francia", nil, nil, nil, nil, "www.elmundo.es")
	
	@revista1=Articuloe.new("nintendo", "nintendo accion", nil, nil, nil, nil, "899869699")
	
	@ref1=Bibliografia.new("Juan", "Titulo", nil, nil, nil, nil)
	
	@nodonil=Nodo.new(nil)
	@nodo0 = Nodo.new("dato",nil)
	@nodo01= Nodo.new("dato",@nodo0)
	@lista= Lista.new()
	
	@nodo1=Nodo.new(@libro1, nil)
	@nodo2=Nodo.new(@libro2, nil)
	@nodo3=Nodo.new(@libro3, nil)
	@nodo4=Nodo.new(@libro4, nil)
	@nodo5=Nodo.new(@libro5, nil)
	@nodo6=Nodo2.new(@libro1,nil,nil)

	
	@lista2=Lista2.new()
	
	end 


context "Nodo" do
	describe "#Debe existir un nodo con su dato y su siguiente"do
		it "Se almacena el dato" do
			expect(@nodo0.n).to eq("dato")
		end
		it "Se almacena el next" do
			expect(@nodo0.next).to eq(nil)
			expect(@nodo01.next).to eq(@nodo0)
	
		end
	
	end
end



context "Lista" do
	describe "#Existe una lista con su inicio"do
		it "Se inserta el nodo 1 en la lista"do
			@lista.insert_first(@nodo1)
			expect(@lista.first).to eq(@nodo1)
		end
	end
	
	describe "#Se extrae el primer elemento de la lista"do
		it "Se extrae el primer nodo"do
			@lista.insert_first(@nodo1)
			expect(@lista.first).to eq(@nodo1)
			expect(@lista.extract_first).to eq(@nodo1)
			expect(@lista.first).to eq(@nodonil)
		end
	end
	
	describe "#Se pueden insertar varios elementos"do
		it "Se insertan los elementos en la lista" do
			@lista.insert_first(@nodo1)
			expect(@lista.first).to eq(@nodo1)
			@lista.insert_next(@nodo1,@nodo2)
			expect(@nodo1.next).to eq(@nodo2)
			@lista.insert_next(@nodo2,@nodo3)
			expect(@nodo2.next).to eq(@nodo3)
			@lista.insert_next(@nodo3,@nodo4)
			expect(@nodo3.next).to eq(@nodo4)
			@lista.insert_next(@nodo4,@nodo5)
			expect(@nodo5.next).to eq(nil)
			expect(@lista.first).to eq(@nodo1)
			
			
		end
	end
end

context "Lista2" do
	
	describe"#Existe un nodo de la lista doble"do
		it"Se almacenan los valores correctamente"do
			expect(@nodo6.next).to eq(nil)
			expect(@nodo6.prev).to eq(nil)
		
		end
	end
	
	describe"#Existe una lista doblemente enlazada con su head y su tail"do
		it"Se inserta un nodo en la lista"do
			@lista2.insert_nodo(@libro1)
			expect(@lista2.head.n).to eq(@libro1)
			expect(@lista2.tail.n).to eq(@libro1)
		end
	end
	
	describe"#Se insertan varios nodos en la lista"do
		it"Se insertan correctamente"do
			
			@lista2.insert_nodo(@libro1)
				expect(@lista2.head.n).to eq(@libro1)
				expect(@lista2.tail.n).to eq(@libro1)
			@lista2.insert_nodo(@libro2)
				expect(@lista2.head.n).to eq(@libro2)
				expect(@lista2.tail.n).to eq(@libro1)
			@lista2.insert_nodo(@libro3)
				expect(@lista2.head.n).to eq(@libro3)
				expect(@lista2.tail.n).to eq(@libro1)
			@lista2.insert_nodo(@libro4)
				expect(@lista2.head.n).to eq(@libro4)
				expect(@lista2.tail.n).to eq(@libro1)
			@lista2.insert_nodo(@libro5)
				expect(@lista2.head.n).to eq(@libro5)
				expect(@lista2.tail.n).to eq(@libro1)
			
		end
	end
end


context "Herencia"do
	@libro1.is_a?Libro
	@libro1.is_a?Bibliografia
	@libro1.instance_of?Libro
	
	@articulo1.is_a?Articuloe
	@articulo1.instance_of?Articuloe
	@articulo1.instance_of?Bibliografia
	
	@revista1.is_a?Revista
	@revista1.instance_of?Revista
	@revista1.instance_of?Bibliografia
	
	@ref1.is_a?Bibliografia
	
end

end